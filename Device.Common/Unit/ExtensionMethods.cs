﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Device.Common
{
    public static class ExtensionMethods
    {
        public static async Task<Pagination<T>> GetPageAsync<T>(this IQueryable<T> dbSet, PaginationRequest query)
         where T : class
        {

            query.Page = query.Page ?? 1;
            if ( query.Size.HasValue)
            {
                var totals =  dbSet.Count();

                if (query.Size == -1)
                    query.Size = totals;

                var totalsPages = (int)Math.Ceiling(totals / (float)query.Size.Value);
                var excludedRows = (query.Page.Value - 1) * query.Size.Value;
                var items =  dbSet.Skip(excludedRows).Take(query.Size.Value).ToList();
                return new Pagination<T>
                {
                    Page = query.Page.Value,
                    Content = items,
                    NumberOfElements = items.Count,
                    Size = query.Size.Value,
                    TotalPages = totalsPages,
                    TotalElements = totals
                };
            }

            if (!query.Size.HasValue)
            {
                var totals = dbSet.Count();
                var items = dbSet.ToList();
                return new Pagination<T>
                {
                    Page = 1,
                    Content = items,
                    NumberOfElements = totals,
                    Size = totals,
                    TotalPages = 1,
                    TotalElements = totals
                };
            }

            return null;
        }

    }
}