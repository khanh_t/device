﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Device.Common
{
    public class PaginationRequest
    {

        [Range(1, int.MaxValue)] public int? Page { get; set; } = 1;

        [Range(1, int.MaxValue)] public int? Size { get; set; } = 20;

    }
}