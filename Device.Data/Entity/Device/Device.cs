﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Device.Data
{
    public class Device
    {
        [BsonId]
        public Guid Id { get; set; }

        public string Name{ get; set; }
    }
}
