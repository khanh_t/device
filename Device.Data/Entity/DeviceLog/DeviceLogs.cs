﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Device.Data
{
   public class DeviceLog
    {
        [BsonId]
        public Guid Id { get; set; }

        public string Content { get; set; }

        public string DeviceName { get; set; }

        public DateTime CreateDate { get; set; } = DateTime.Now;

        public int ClockDiff { get; set; }

        public DateTime DeviceTime { get; set; } = DateTime.Now;

        public Guid DeviceId { get; set; }

    }
}
