﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Device.Data
{
  public  class dvDbContext
    {
        private readonly IMongoDatabase _db;

        public dvDbContext(IMongoClient client, string dbName)
        {
            _db = client.GetDatabase(dbName);
        }

        public IMongoCollection<Device> Device => _db.GetCollection<Device>("Device");
        public IMongoCollection<DeviceLog> DeviceLog => _db.GetCollection<DeviceLog>("DeviceLog");
    }
}
