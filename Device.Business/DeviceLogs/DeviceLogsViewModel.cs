﻿using Device.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Device.Business
{
   public class DeviceLogViewModel
    {
        public Guid Id { get; set; }

        public string Content { get; set; }

        public string DeviceName { get; set; }

        public DateTime CreateDate { get; set; } = DateTime.Now;

        public int ClockDiff { get; set; }

        public DateTime DaviceTime { get; set; } = DateTime.Now;

        public Guid DeviceId { get; set; }
    }

    public class DeviceLogCreateModel
    {

        public string Content { get; set; }

        public string DeviceName { get; set; }

        public int ClockDiff { get; set; }

        public Guid DeviceId { get; set; }
    }

    public class DeviceLogQueryModel:PaginationRequest
    {
        public DateTime FromDate { get; set; } = new DateTime(1970, 1, 1);
        public DateTime ToDate { get; set; } = DateTime.Now;
    }


}
