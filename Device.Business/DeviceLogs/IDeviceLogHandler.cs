﻿using Device.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Device.Business
{
   public interface IDeviceLogHandler
    {
        Task<Response> Create(int quantity);
        Task<Response> GetPageAsync(DeviceLogQueryModel query,Guid deviceId);
    }
}
