﻿using AutoMapper;
using Bogus;
using Device.Common;
using Device.Data;
using LinqKit;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Device.Business
{
    public class DeviceLogHandler : IDeviceLogHandler
    {
        private readonly dvDbContext _db;
        private readonly IMapper _mapper;
        public DeviceLogHandler(dvDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<Response> Create(int quantity)
        {
            try
            {
                var device = await _db.Device.Find(c => true).ToListAsync();

                var devicelog = new Faker<DeviceLog>().RuleFor(o => o.Id, f => f.Random.Guid())
                    .RuleFor(o => o.DeviceName, f => f.PickRandom(device).Name)
                    .RuleFor(o => o.Content, f => f.Lorem.Sentence())
                    .RuleFor(o => o.ClockDiff, f => f.Random.Number(1, 1000))
                    .RuleFor(o => o.DeviceTime, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now))
                    .RuleFor(o => o.CreateDate, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now))
                    .RuleFor(o => o.DeviceId, f => f.PickRandom(device).Id)
                    .Generate(quantity);

                await _db.DeviceLog.InsertManyAsync(devicelog);

                return new Response(HttpStatusCode.OK, $"Thêm {quantity} bản ghi thành công !!!");
            }
            catch (Exception ex)
            {

                Log.Error(ex, "Thêm record không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }


        }

        public async Task<Response> GetPageAsync(DeviceLogQueryModel query, Guid deviceId)
        {
            try
            {
                var device = await _db.Device.Find(x => x.Id == deviceId).FirstOrDefaultAsync();

                if (device == null)
                {
                    return new Response(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
                }

                var predicate = PredicateBuilder.New<DeviceLog>(true);
                if (deviceId != null && deviceId != Guid.Empty)
                {
                    predicate.And(b => b.DeviceId == deviceId);
                }

                if (query.FromDate != null && query.ToDate != null)
                {
                    predicate.And(b => b.CreateDate >= query.FromDate && b.CreateDate <= query.ToDate);
                }

                var deviceplog = _db.DeviceLog;

                var queryResult = deviceplog.AsQueryable().Where(predicate).OrderBy(x => x.CreateDate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<DeviceLogViewModel>>(data);

                if (result != null) return new ResponsePagination<DeviceLogViewModel>(result);

                else return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách mặt hàng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}
