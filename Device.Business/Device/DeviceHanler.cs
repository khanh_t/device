﻿using AutoMapper;
using Device.Common;
using Device.Data;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Device.Business
{
    public class DeviceHanler : IDeviceHanler
    {
        private readonly dvDbContext _db;
        private readonly IMapper _mapper;
        public DeviceHanler(dvDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<Response> Create(DeviceCreateModel model)
        {
          var device=  _mapper.Map<Data.Device>(model);
             await _db.Device.InsertOneAsync(device);
             var result=  _mapper.Map<Data.Device,DeviceViewModel>(device);
            return new Response<DeviceViewModel>(result);
        }

        public async Task<Response> Get()
        {
            var device = await _db.Device.Find(c => true).ToListAsync();
            var result = _mapper.Map< List<DeviceViewModel>>(device);
            return new ResponseList<DeviceViewModel>(result);
        }
    }
}
