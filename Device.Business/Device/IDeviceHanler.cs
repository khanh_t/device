﻿using Device.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Device.Business
{
   public interface IDeviceHanler
    {
        Task<Response> Create(DeviceCreateModel model);
        Task<Response> Get();
    }
}
