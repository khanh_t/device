﻿using AutoMapper;
using Device.Business;
using Device.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Device.AutoMapperConfig
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            CreateMap<Data.Device,DeviceViewModel > ().ReverseMap();
            CreateMap<DeviceCreateModel, Data.Device>().ReverseMap();
            CreateMap<Data.DeviceLog, DeviceLogViewModel>().ReverseMap();
            CreateMap<DeviceLogCreateModel, Data.DeviceLog>().ReverseMap();
        }

    }
}
