﻿using Device.Business;
using Device.Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Device.Controllers
{
    [ApiController]
    [Route("api/v1/devicelog")]
    public class DeviceLogController : ControllerBase
    {
        private readonly IDeviceLogHandler _service;

        public DeviceLogController(IDeviceLogHandler service)
        {
            _service = service;
        }

        [HttpPost, Route("")]
        public async Task<IActionResult> Create([FromQuery] int quantity)
        {

            var result = await _service.Create(quantity);

            return Helper.TransformData(result);
        }

        [HttpGet, Route("{deviceId}")]
        public async Task<IActionResult> Get(DateTime fromDate, DateTime toDate,Guid deviceId,[FromQuery] int size = 20, [FromQuery] int page = 1)
        {
            var startDay= new DateTime(0001, 1, 1);
            var filterObject = new DeviceLogQueryModel();
            filterObject.FromDate = (fromDate != startDay) ? fromDate : filterObject.FromDate;
            filterObject.ToDate = (toDate != startDay) ? toDate : filterObject.ToDate;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _service.GetPageAsync(filterObject,deviceId);

            return Helper.TransformData(result);
        }

    }
}
