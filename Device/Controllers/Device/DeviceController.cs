﻿using Device.Business;
using Device.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Device.Controllers.Device
{
    [ApiController]
    [Route("api/v1/device")]
    public class DeviceController :ControllerBase
    {
        private readonly IDeviceHanler _service;

        public DeviceController(IDeviceHanler service)
        {
            _service = service;
        }

        [HttpPost, Route("")]      
        public async Task<IActionResult> Create([FromBody] DeviceCreateModel model)
        {
         
            var result = await _service.Create(model);

            return Helper.TransformData(result);
        }

        [HttpGet, Route("")]
        public async Task<IActionResult> GetPageAsync()
        {

            var result = await _service.Get();

            return Helper.TransformData(result);
        }
    }
}
